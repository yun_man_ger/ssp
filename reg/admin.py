from django.contrib import admin
from reg.models import *
from files.models import UserFile
from anketa.models import Anketa
from discussion.models import *


class AnketaAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'course', 'mail', 'mobile', 'date', 'google_docs_link')
    date_hierarchy = 'date'
    list_filter = ('course',)

admin.site.register(Client)
admin.site.register(Discussion)
admin.site.register(Comment)
admin.site.register(UserFile)
admin.site.register(Anketa, AnketaAdmin)
admin.site.register(Entry)
