#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from django.contrib.sites.models import Site
from django.db import models
from django.utils.safestring import mark_safe


# from django.forms import ModelForm
# from django import forms

class Anketa(models.Model):

    TITLE_CHOICES = ((1, 'first'), (2, 'second'), (3, 'third'), (4,
                     'fourth'))
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    course = models.DecimalField(max_digits=2, decimal_places=1,
                                 choices=TITLE_CHOICES)
    mail = models.EmailField()
    mobile = models.CharField(max_length=15)
    rezume = models.FileField(upload_to='userdata/anketa/')
    date = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return self.name

    def google_docs_link(self):
        return self.rezume \
            and mark_safe('<a href="http://docs.google.com/viewer?url=%s">%s</a>'
                           % (''.join(('http://',
                          Site.objects.get_current().domain,
                          self.rezume.url)),
                          os.path.basename(self.rezume.path))) or 'None'

    google_docs_link.short_description = 'Google Docs Link'
    google_docs_link.allow_tags = True
