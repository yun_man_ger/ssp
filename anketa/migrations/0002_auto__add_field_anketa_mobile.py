# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Anketa.mobile'
        db.add_column('anketa_anketa', 'mobile',
                      self.gf('django.db.models.fields.CharField')(default='No number', max_length=15),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Anketa.mobile'
        db.delete_column('anketa_anketa', 'mobile')


    models = {
        'anketa.anketa': {
            'Meta': {'object_name': 'Anketa'},
            'course': ('django.db.models.fields.DecimalField', [], {'max_digits': '2', 'decimal_places': '1'}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'mobile': ('django.db.models.fields.CharField', [], {'default': "'No number'", 'max_length': '15'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'rezume': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['anketa']